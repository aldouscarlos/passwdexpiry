##  Below are the details of how the shell script and the log file were deployed, permissions and location, including crontab entry.  THESE ARE DEPLOYED IN SEDB ONLY.
##
##
sudo touch /var/log/chkexpirydate.log
sudo chown root.adm /var/log/chkexpirydate.log
sudo chmod 666 /var/log/chkexpirydate.log
sudo touch /usr/local/bin/daysexpire.sh
sudo chown root.adm /usr/local/bin/daysexpire.sh
sudo chmod 755 /usr/local/bin/daysexpire.sh
sudo vi /usr/local/bin/daysexpire.sh
##
##
##
##

sysadmin@syd2sema001sedb:~$ cat /usr/local/bin/daysexpire.sh
#!/bin/bash
#
#VARIABLES
datetoday=`date +%s`
expireday=0
daycount=0
datetxt=''
logfile=/var/log/chkexpirydate.log
#
#
#FUNCTIONS
countexpiry() {
datetxt=`chage -l sysadmin|grep 'Password expires' |cut -f2 -d':'`
expireday=`date -d "$datetxt" +%s`
daycount=$(((datetoday - expireday)/60/60/24))
echo "Days (+)pass expiry / (-)until expiry: "$daycount  > $logfile

return 0
}
#
#
logit() {
logger -f $logfile

return 0
}
#
#
#
#MAIN PROG
countexpiry
logit


exit 0



##
##
##
##
sysadmin@syd2sema001sedb:~$ ls -ltr /var/log/chkexpirydate.log
-rw-rw-rw- 1 root adm 43 Sep  9 06:01 /var/log/chkexpirydate.log
##
##
sysadmin@syd2sema001sedb:~$ ls -ltr /usr/local/bin/daysexpire.sh
-rwxr-xr-x 1 root adm 465 Sep  7 14:51 /usr/local/bin/daysexpire.sh
##
##
##
# m h  dom mon dow   command
01 06 * * * /usr/local/bin/daysexpire.sh > /dev/null 2>&1